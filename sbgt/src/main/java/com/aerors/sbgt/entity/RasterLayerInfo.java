package com.aerors.sbgt.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RasterLayerInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String name;
	private String url;
	private Date importDate;// 入库日期
	private Date photoDate;// 拍摄日期
	private String bound;// x1,y1,x2,y2

	public RasterLayerInfo() {
		super();
	}

	public RasterLayerInfo(String name, String url) {
		super();
		this.name = name;
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public Date getPhotoDate() {
		return photoDate;
	}

	public void setPhotoDate(Date photoDate) {
		this.photoDate = photoDate;
	}

	public String getBound() {
		return bound;
	}

	public void setBound(String bound) {
		this.bound = bound;
	}

	@Override
	public String toString() {
		return "RasterLayerInfo [name=" + name + ", url=" + url + ", importDate=" + importDate + ", photoDate="
				+ photoDate + ", bound=" + bound + "]";
	}
}
