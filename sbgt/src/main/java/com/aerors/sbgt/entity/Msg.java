package com.aerors.sbgt.entity;

public class Msg {
	private int status;//1 for success,0 for failure
	private String info;
	private Object obj;

	public Msg() {
		super();
	}

	public Msg(int status, String info) {
		super();
		this.status = status;
		this.info = info;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}
