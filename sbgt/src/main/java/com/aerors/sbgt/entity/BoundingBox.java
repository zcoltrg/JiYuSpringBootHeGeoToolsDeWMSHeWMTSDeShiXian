package com.aerors.sbgt.entity;

public class BoundingBox {
	public double north;
	public double south;
	public double east;
	public double west;
}
