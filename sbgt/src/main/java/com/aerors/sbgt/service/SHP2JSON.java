package com.aerors.sbgt.service;

import java.io.File;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.FeatureWriter;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.json.JSONArray;
import org.json.JSONObject;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.springframework.stereotype.Service;

import com.aerors.sbgt.entity.Msg;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

@Service
public class SHP2JSON {
	public Map geojson2Shape(String jsonPath, String shpPath) {
		Map map = new HashMap();
		GeometryJSON gjson = new GeometryJSON();
		try {
			String strJson = "";
			JSONObject json = new JSONObject(strJson);
			JSONArray features = (JSONArray) json.get("features");
			JSONObject feature0 = new JSONObject(features.get(0).toString());
			System.out.println(feature0.toString());
			String strType = ((JSONObject) feature0.get("geometry")).getString("type").toString();

			Class<?> geoType = null;
			switch (strType) {
			case "Point":
				geoType = Point.class;
			case "MultiPoint":
				geoType = MultiPoint.class;
			case "LineString":
				geoType = LineString.class;
			case "MultiLineString":
				geoType = MultiLineString.class;
			case "Polygon":
				geoType = Polygon.class;
			case "MultiPolygon":
				geoType = MultiPolygon.class;
			}
			// 创建shape文件对象
			File file = new File(shpPath);
			Map<String, Serializable> params = new HashMap<String, Serializable>();
			params.put(ShapefileDataStoreFactory.URLP.key, file.toURI().toURL());
			ShapefileDataStore ds = (ShapefileDataStore) new ShapefileDataStoreFactory().createNewDataStore(params);
			// 定义图形信息和属性信息
			SimpleFeatureTypeBuilder tb = new SimpleFeatureTypeBuilder();
			tb.setCRS(DefaultGeographicCRS.WGS84);
			tb.setName("shapefile");
			tb.add("the_geom", geoType);
			tb.add("POIID", Long.class);
			ds.createSchema(tb.buildFeatureType());
			// 设置编码
			Charset charset = Charset.forName("GBK");
			ds.setCharset(charset);
			// 设置Writer
			FeatureWriter<SimpleFeatureType, SimpleFeature> writer = ds.getFeatureWriter(ds.getTypeNames()[0],
					Transaction.AUTO_COMMIT);

			for (int i = 0, len = features.length(); i < len; i++) {
				String strFeature = features.get(i).toString();
				Reader reader = new StringReader(strFeature);
				SimpleFeature feature = writer.next();
				feature.setAttribute("the_geom", gjson.readMultiPolygon(reader));
				feature.setAttribute("POIID", i);
				writer.write();
			}
			writer.close();
			ds.dispose();
			map.put("status", "success");
			map.put("message", shpPath);
		} catch (Exception e) {
			map.put("status", "failure");
			map.put("message", e.getMessage());
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * shp转换为Geojson
	 * 
	 * @param shpPath
	 * @return
	 */
	public Msg shape2Geojson(String shpPath) {
		Msg msg = new Msg();

		FeatureJSON fjson = new FeatureJSON();

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("{\"type\": \"FeatureCollection\",\"features\": ");

			File file = new File(shpPath);
			ShapefileDataStore shpDataStore = null;

			shpDataStore = new ShapefileDataStore(file.toURI().toURL());
			// 设置编码
			Charset charset = Charset.forName("GBK");
			shpDataStore.setCharset(charset);
			String typeName = shpDataStore.getTypeNames()[0];
			SimpleFeatureSource featureSource = null;
			featureSource = shpDataStore.getFeatureSource(typeName);
			SimpleFeatureCollection result = featureSource.getFeatures();
			SimpleFeatureIterator itertor = result.features();
			JSONArray array = new JSONArray();
			while (itertor.hasNext()) {
				SimpleFeature feature = itertor.next();
				StringWriter writer = new StringWriter();
				fjson.writeFeature(feature, writer);
				JSONObject json = new JSONObject(writer.toString());
				array.put(json);
			}
			itertor.close();
			sb.append(array.toString());
			sb.append("}");

			msg.setStatus(1);
			msg.setObj(sb.toString());
			msg.setInfo("success");
		} catch (Exception e) {
			msg.setStatus(0);
			msg.setInfo("fail:"+e.getMessage());
			e.printStackTrace();
		}
		return msg;
	}
}
