package com.aerors.sbgt.service;

import java.io.File;
import java.util.List;

import org.geotools.map.MapContent;
import org.springframework.stereotype.Service;

import com.aerors.sbgt.entity.TileInfo;
import com.aerors.sbgt.util.GtHelper;
import com.aerors.sbgt.util.TileCalculator;

/**
 * 生成wmts切片
 * 
 * @author tianhui
 *
 */
@Service
public class TileGenerator {
	private static final int scaleMin = 10; 
	private static final int scaleMax = 11; 
	private static final String basePath = "D:\\workData\\tiles";
	private static List<TileInfo> tileMatrix;
	
	public static void main(String[] args) {
		TileGenerator tg = new TileGenerator();
		tg.generateTile("D:\\智慧城管\\haerbin\\2011_output_cai.tif");
	}

	public void generateTile(String fileName) {
		GtHelper gtHelper = GtHelper.getInstance();
		TileCalculator tileCalculator = TileCalculator.getInstance();
		
		MapContent map = gtHelper.addRasterToMap(fileName);
		
		double lonMax = map.getMaxBounds().getMaxX();
		double lonMin = map.getMaxBounds().getMinX();
		double latMax = map.getMaxBounds().getMaxY();
		double latMin = map.getMaxBounds().getMinY();
		
		tileMatrix = tileCalculator.getOSMTileMatrix(scaleMin, scaleMax, latMin, latMax, lonMin, lonMax);
	
		for (TileInfo tileInfo : tileMatrix) {
			String pngFolderPath = basePath + File.separator + tileInfo.scale + File.separator + tileInfo.rowNumber;
			String pngPath = pngFolderPath + File.separator + tileInfo.columnNumber + ".png";

			File pngFolder = new File(pngFolderPath);
			if (!pngFolder.exists()) {
				pngFolder.mkdirs();
			}
			System.out.println("pngPath:" + pngPath);

			gtHelper.generateTile(map, pngPath, tileInfo.bbox);
		}
//		map.dispose();
	}

}
