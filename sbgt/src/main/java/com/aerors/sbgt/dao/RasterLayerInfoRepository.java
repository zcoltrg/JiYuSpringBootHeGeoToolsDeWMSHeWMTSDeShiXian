package com.aerors.sbgt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aerors.sbgt.entity.RasterLayerInfo;

public interface RasterLayerInfoRepository extends JpaRepository<RasterLayerInfo, Long> {
	Boolean existsByName(String name);

	List<RasterLayerInfo> findByName(String name);
}
